use v6.c;

use Terminal::ANSIColor;
use Text::BorderedBlock;
use Pod::To::Pager::Superscripter;

#| A pod parser class, targeted towards users in a shell with a pager available
#| to them.
class Pod::To::Pager
{
	#| The maximum length for lines.
	my Int $line-length = 80;

	#| The size of an indent. For best results, use an even number here.
	my Int $indent = 8;

	#| The bullet characters to use for list items. List items that go deeper
	#| than this array has bullets for will wrap around.
	my Str @bullets = < • - + >;

	#| When receiving multiple pod blocks, handle only the named blocks. This
	#| will filter out any declarator blocks, which most end-users won't care
	#| about in the first place. From those named blocks, only deal with the
	#| first block.
	multi method render(
		@pod is copy,
		:@footnotes = [],
		Bool:D :$return = False,
	) {
		@pod .= grep({ $_.WHAT ~~ Pod::Block::Named }) if (1 < @pod.elems);

		my Str $document = self.render(@pod[0]);

		return $document if $return;

		print "$document\n";
		return;
	}

	#| If no valid object is given, it can't be rendered. This should only
	#| happen if render(@) did not yield a sane result.
	multi method render(
		Any:U,
		:@footnotes = [],
	) {
		say "No usable Perl 6 Pod found in the given file";
		exit 1;
	}

	#| Fallback handler.
	multi method render(
		Any:D $pod,
		:@footnotes = [],
		--> Str
	) {
		note "Unsupported pod structure:";
		dd $pod;

		"";
	}

	#| Render a C<=being code> pod block.
	multi method render (
		Pod::Block::Code:D $pod,
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";
		my Text::BorderedBlock $block .= new;

		$contents ~= self.render($_, :@footnotes) for $pod.contents;
		$contents = $block.render(
			$contents,
			header => $pod.config<name> // "",
		);

		$contents.lines.map(*.indent($indent * 1.5)).join("\n") ~ "\n\n";
	}

	#| Fallback for named pod block rendering.
	multi method render (
		Pod::Block::Named:D $pod,
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("bold blue") ~ $pod.name.uc ~ color("reset") ~ "\n";

		$contents ~= self.render($_, :@footnotes) for $pod.contents;
		$contents .= trim-trailing;
		$contents ~= "\n\n" ~ self!render-footnotes(:@footnotes) if @footnotes;

		$contents ~ "\n";
	}

	#| Render a C<=defn> pod block.
	multi method render (
		Pod::Block::Named:D $pod where *.name eq "defn",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("cyan") ~ $pod.name.indent($indent) ~ color("reset") ~ "\n";

		$contents ~= self.render($_, :@footnotes).trim.indent($indent + 2) for $pod.contents;

		$contents ~ "\n\n";
	}

	#| Render a C<=begin pod> block.
	multi method render (
		Pod::Block::Named:D $pod where *.name eq "pod",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";

		$contents ~= self.render($_, :@footnotes) for $pod.contents;
		$contents .= trim-trailing;
		$contents ~= "\n\n" ~ self!render-footnotes(:@footnotes) if @footnotes;

		$contents.trim ~ "\n";
	}

	#| Render a C<=begin input> block.
	multi method render (
		Pod::Block::Named:D $pod where *.name eq "input",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";
		my Text::BorderedBlock $block .= new;

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents = $contents.lines.map({ color("yellow") ~ $_.trim ~ color("reset") }).join("\n");
		$contents = $block.render($contents);

		$contents.lines.map(*.indent($indent * 1.5)).join("\n") ~ "\n\n";
	}

	#| Render a C<=output> block.
	multi method render (
		Pod::Block::Named:D $pod where *.name eq "output",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";
		my Text::BorderedBlock $block .= new;

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents = $contents.lines.map({ color("magenta") ~ $_.trim ~ color("reset") }).join("\n");
		$contents = $block.render($contents);

		$contents.lines.map(*.indent($indent * 1.5)).join("\n") ~ "\n\n";
	}

	#| Render a paragraph.
	multi method render (
		Pod::Block::Para:D $pod,
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";
		my Str @lines = [];
		my Str @words = [];

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		return $contents if $line-length < 1;

		for $contents.split(" ") -> $word {
			if ((colorstrip(@words.join(" ")).chars + colorstrip($word).chars) > $line-length) {
				@lines.append: @words.join(" ");
				@words = [];
			}

			@words.append: $word;
		}

		@lines.append: @words.join(" ");
		@lines.map(*.indent($indent)).join("\n") ~ "\n\n";
	}

	#| Fallback for formatting codes.
	multi method render (
		Pod::FormattingCode:D $pod,
		:@footnotes = [],
		--> Str
	) {
		note "Unsupported Pod::FormattingCode: $pod.type()";

		my Str $contents = "";

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents;
	}

	#| Render a bold formatted pod block.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "B",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("bold");

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents ~ color("reset");
	}

	#| Render a code formatted pod block.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "C",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("bold");

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents ~ color("reset");
	}

	#| Render an HTML entity.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "E",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents;
	}

	#| Render an italic formatted pod block.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "I",
		:@footnotes = [],
		--> Str
	) {
		# TODO: Replace ITALIC with color(italic) once Terminal::ANSIColor gets patched
		my Str $contents = ITALIC;

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents ~ color("reset");
	}

	#| Render keyboard input.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "K",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("yellow");
		my Int $current-line-length = $line-length;

		$line-length = 0;

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$line-length = $current-line-length;

		$contents ~ color("reset");
	}

	#| Render a link.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "L",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("underline") ~ $pod.meta ~ color("reset");

		$contents ~= " <" ~ color("blue");
		$contents ~= self.render($_, :@footnotes) for $pod.contents;
		$contents ~= color("reset") ~ ">";

		$contents;
	}

	#| Add an additional footnote from a footnode pod block.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "N",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		@footnotes.append($contents);

		superscript-digits(@footnotes.elems);
	}

	#| Render terminal output.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "T",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("magenta");

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents ~ color("reset");
	}

	#| Render a pod block with an underline.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "U",
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = color("underline");

		$contents ~= self.render($_, :@footnotes) for $pod.contents;

		$contents ~ color("reset");
	}

	#| Hide comments.
	multi method render (
		Pod::FormattingCode:D $pod where *.type eq "Z",
		:@footnotes = [],
		--> Str
	) {
		"";
	}

	#| Render a pod heading.
	multi method render (
		Pod::Heading:D $pod,
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";
		my Str $footnotes = "";

		# Render footnotes from previous block
		$footnotes ~= self!render-footnotes(:@footnotes) ~ "\n" if @footnotes;

		my Int $current-indent = $indent;

		$indent = ($pod.level - 1) * 2;

		$contents ~= self.render($_, :@footnotes).uc for $pod.contents;

		$indent = $current-indent;

		$contents = color("bold blue") ~ $contents.trim-trailing ~ color("reset");

		"$footnotes\n$contents\n";
	}

	#| Render a list item
	multi method render (
		Pod::Item:D $pod,
		:@footnotes = [],
		--> Str
	) {
		my Str $contents = "";
		my Int $current-indent = $indent;
		my Int $current-line-length = $line-length;
		my Str $bullet = @bullets[($pod.level - 1) % @bullets.elems] ~ " ";

		$indent = $pod.level × 2 + floor($current-indent / 2);
		$line-length = $current-line-length - $indent - $bullet.chars;

		my Str @lines = [];

		@lines.append(self.render($_, :@footnotes).lines) for $pod.contents;

		$contents ~= $bullet.indent($indent) ~ @lines.shift.trim ~ "\n\n";

		if (@lines) {
			$contents ~= @lines.map(*.trim).map(*.indent($indent + $bullet.chars)).join("\n");
		}

		$line-length = $current-line-length;
		$indent = $current-indent;

		$contents;
	}

	#| Render a string.
	multi method render (
		Str:D $text,
		:@footnotes = [],
		--> Str
	) {
		$text;
	}

	#| Create the footnotes text block from an array of C<@footnotes>. The
	#| C<@footnotes> will be emptied after the block has been generated.
	method !render-footnotes (
		:@footnotes = [],
		--> Str
	) {
		return "" unless @footnotes;

		my Str $contents ~= "{'_' x 15}\n".indent($indent);

		for @footnotes.kv -> $key, $value {
			$contents ~= "{$key + 1}: $value\n".indent($indent);
		}

		@footnotes = [];
		$contents.trim-trailing;
	}
}

=begin pod

=NAME    Pod::To::Pager
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.2.0
=LICENSE AGPL-3.0

=head1 Synopsis

Pod::To::Pager.render($pod);

=head1 Description

Pod::To::Pager is a pod parser to generate output targeted towards users in a
shell. It comes with the C<p6man> as a convenience utility to read the pod
contents of a Perl 6 source file.

=head1 Examples

=head2 Generating documentation on a Perl 6 file

=begin input
zef install Pod::To::Pager
perl6 --doc=Pager program.pl
=end input

=head2 Generating documentation from within a Perl 6 program

=begin input
use Pod::To::Pager;

Pod::To::Pager.render($=pod);
=end input

=head1 See also

=item1 p6man

=end pod

# vim: ft=perl6 noet
