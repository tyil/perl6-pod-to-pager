#! /usr/bin/env false

use v6.c;

unit module Pod::To::Pager::ModuleResolver;

#| Resolve a module name to the path it's installed to.
sub resolve-module (
	#| Name of the module.
	Str:D $module,
	--> Str
) is export {
	my Proc $zef = run << zef locate "$module" >>, :out;

	for $zef.out.lines -> $line {
		next unless $line ~~ / \S+ " => " (\S+) /;

		return ~$0;
	}

	return "";
}
