#! /usr/bin/env false

use v6.c;

unit module Pod::To::Pager::Superscripter;

multi sub superscript-digits (
	Int:D $digits,
	--> Str
) is export {
	superscript-digits(~$digits);
}

multi sub superscript-digits (
	Str:D $digits,
	--> Str
) is export {
	$digits.uninames.map(*.subst(/^DIGIT/, "SUPERSCRIPT").uniparse).join();
}

# vim: ft=perl6 noet
