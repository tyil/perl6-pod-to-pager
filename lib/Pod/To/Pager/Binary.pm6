#! /usr/bin/env false

use v6.c;

use Pod::To::Pager::ModuleResolver;

unit module Pod::To::Pager::Binary;

#| Render a local file.
multi sub run-p6man (
	Str:D $path,
	Bool:D :$skip-pager = False,
	Bool:D :$try-resolving where !*,
	--> Int
) is export {
	my Str $command = "'$*EXECUTABLE' --doc=Pager '$path'";

	if (!$skip-pager) {
		my Str $pager = %*ENV<PAGER> // ($*DISTRO.is-win ?? 'more' !! 'less -r');

		$command ~= " | $pager";
	}

	shell $command;

	exit 0;
}

#| Render an installed module.
multi sub run-p6man (
	Str:D $module,
	Bool:D :$skip-pager = False,
	Bool:D :$try-resolving where *,
	--> Int
) is export {
	my Str $path = resolve-module($module);

	# Make sure a result was found by resolve-module
	if (!$path) {
		note "No such module or file: $module";
		return 2;
	}

	# Check for external pod
	my @extensions = [
		"pod",
        "pod6",
    ];

	for @extensions {
		my IO::Path:D $alternative = $path.IO.extension($_);

		return run-p6man($alternative.absolute, :$skip-pager, :!try-resolving) if $alternative.e;
	}

	# Run p6man with the found path
	run-p6man($path, :$skip-pager, :!try-resolving);
}
