# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [UNRELEASED]
### Added
- Giving `p6man` the name of an installed module will attempt to look it up and
  render the pod document embedded inside it.

### Changed
- Links now have their URL displayed after the shown text.
- Footnotes now appear closer to the place they were created in. This generally
  means before the next named block or heading.
- Footnotes are now indicated by superscript numbers, instead of numbers
  inbetween brackets.

## [0.2.0] - 2018-06-24
### Added
- Add pod structure to `p6man`
- Add pod structure to `Pod::To::Pager::BorderedBlock`
- Added a test case to ensure rendering works

### Changed
- Headings will now be indented, based on their level

## [0.1.0] - 2018-06-23
- Initial release
