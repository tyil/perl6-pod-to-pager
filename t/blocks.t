#! /usr/bin/env perl6

use v6.c;

use Test;
use Pod::To::Pager;

my Str @files = [
	"test-block-input-with-description",
];

plan @files.elems;

for @files -> $file {
	subtest "Running $file" => {
		plan 1;

		my Proc $p6man = run « p6man --skip-pager "t/sources/$file.pod" », :out;
		my Str $stdout = $p6man.out.slurp(:close);

		is $stdout, slurp("t/results/$file.txt"), "Generated output is correct";
	}
}

# vim: ft=perl6 noet
