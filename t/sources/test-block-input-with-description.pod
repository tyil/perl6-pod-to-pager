=begin pod

=begin input
do some command
=end input

Here, we show how to use C<some command>. While that generally goes fine, the
descriptions that go with the examples often contain I<some form of markup> in
them. And that's where things start going wrong. This test case should help in
finding B<most> of the bugs related to them, and assist in squashing them U<way>
before they end up screwing something over.

=end pod
