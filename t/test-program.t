#! /usr/bin/env perl6

use v6.c;

use Test;

BEGIN plan :skip-all<Author testing not enabled> unless %*ENV<AUTHOR_TESTING>;

use Pod::To::Pager;

my Str @files = [
	"blocks",
	"test-program",
];

plan @files.elems;

for @files -> $file {
	subtest "Checking $file" => {
		plan 1;

		my Proc $p6man = run « p6man --skip-pager t/sources/test-program.pl », :out;
		my Str $stdout = $p6man.out.slurp(:close);

		is $stdout, slurp("t/results/test-program.txt"), "Generated output is correct";
	}
}

# vim: ft=perl6 noet
